'use strict';

/*
* Require the path module
*/
const path = require('path');

/*
 * Require the Fractal module
 */
const fractal = module.exports = require('@frctl/fractal').create();

/*
 * Give your project a title.
 */
fractal.set('project.title', 'A-Team Web UI styleguide');

/**
 * Any other project metadata that you may want to store for use in you app can be set on project.* keys as required.
 * For instance you may want to keep a reference to your project version and author:
 */
fractal.set('project.version', 'v1.0');
fractal.set('project.author', 'A-Team Solutions');

/*
 * Tell Fractal where to look for components.
 */
fractal.components.set('path', path.join(__dirname, 'base'));
fractal.components.set('path', path.join(__dirname, 'components'));

/*
 * Tell Fractal where to look for documentation pages.
 */
fractal.docs.set('path', path.join(__dirname, 'docs'));

/*
 * Tell the Fractal web preview plugin where to look for static assets.
 */
fractal.web.set('static.path', path.join(__dirname, 'static'));

fractal.web.set('builder.dest', __dirname + '/build');

/**
 * If you want to prefix the URL path of the assets that are being served up, you can do so with the static.mount option:
 * @example public/bar/foo.css will be served at http://localhost:3000/project-assets/bar/foo.css
 */
// fractal.web.set('static.mount', 'project-assets');

/**
 * Hard-code a port number to start the server on. Generally this option is not required
 * because Fractal will automatically find an available port to use.
 */
// fractal.web.set('server.port', 4444);

const mandelbrot = require('@frctl/mandelbrot');
const theme = mandelbrot({
    skin: "white", // aqua | black | blue | default | fuchsia | green | grey | lime | maroon | navy | olive | orange | purple | red | teal | white | yellow
    // panels: ["html", "info", "resources"]
});
fractal.web.theme(theme);
