# A-Team Styleguide

Styleguide & Component library.

Built by [Fractal](https://fractal.build/)

## Installation

```sh
npm i
```

## Development

To start development:

```sh
npm start
```

Building static fractal website (website can be found in `./build`)

```sh
npm run build:fractal
```

Building css (css can be found in `./dist`)

```sh
npm run dist:styles
```
