"use strict";

module.exports = {
    context: {
        column1: {
            title: "Pages",
            list: [
                { label: "Home", link: "#" },
                { label: "Products", link: "#" },
                { label: "Company", link: "#" }
            ],
        },
        column2: {
            title: "Company",
            list: [
                { label: "Contact Us", link: "#" },
                { label: "About Us", link: "#" },
                { label: "More ..", link: "#" }
            ]
        },
        column3: {
            title: "External",
            list: [
                { label: "Peryl", link: "#" },
                { label: "React SPA", link: "#" },
                { label: "Ignite", link: "#" }
            ]
        }
    }
}
