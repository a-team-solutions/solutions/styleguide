"use strict";

const faker = require("faker");

const memberCount = 10;
const memberData = [];

for (var i = 0; i < memberCount; i++) {
    memberData.push({
        name: faker.name.findName(),
        email: faker.internet.email(),
        avatar: faker.internet.avatar()
    });
}

module.exports = {
    status: "wip",
    context: {
        label: "Admin"
    }
};
